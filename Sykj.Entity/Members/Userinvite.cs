/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：推广表                                                    
*│　作    者：bjg                                              
*│　版    本：1.0   模板代码自动生成                                              
*│　创建时间：2019-04-03 10:51:43 
*│  公    司：云南尚雅科技文化有限公司
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间: Sykj.Entity                                  
*│　类    名：Userinvite                                     
*└──────────────────────────────────────────────────────────────┘
*/
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sykj.Entity
{
	/// <summary>
	/// 推广表
	/// </summary>
	public class Userinvite
	{
		/// <summary>
		/// 主键id
		/// </summary>
		public Int32 Id {get;set;}

		/// <summary>
		/// 用户ID（被邀请用户）
		/// </summary>
		[Required(ErrorMessage = "请输入用户ID（被邀请用户）")]
		public Int32 UserId {get;set;}

		/// <summary>
		/// 用户昵称（被邀请用户）
		/// </summary>
		[Required(ErrorMessage = "请输入用户昵称（被邀请用户）")]
		[MaxLength(255)]
		public String UserNick {get;set;}

		/// <summary>
		/// 邀请用户ID
		/// </summary>
		[Required(ErrorMessage = "请输入邀请用户ID")]
		public Int32 InviteUserId {get;set;}

		/// <summary>
		/// 邀请用户昵称
		/// </summary>
		[Required(ErrorMessage = "请输入邀请用户昵称")]
		[MaxLength(255)]
		public String InviteNick {get;set;}

		/// <summary>
		/// 级别
		/// </summary>
		public int Depth {get;set;}

		/// <summary>
		/// 级别路径
		/// </summary>
		[Required(ErrorMessage = "请输入级别路径")]
		[MaxLength(255)]
		public String Path {get;set;}

		/// <summary>
		/// 状态 0:不启用  1:启用
		/// </summary>
		public int Status {get;set;}

		/// <summary>
		///  是否已返利
		/// </summary>
		[Required(ErrorMessage = "请输入 是否已返利")]
		public Boolean IsRebate {get;set;}

		/// <summary>
		/// 是否是新用户
		/// </summary>
		[Required(ErrorMessage = "请输入是否是新用户")]
		public Boolean IsNew {get;set;}

		/// <summary>
		/// 创建时间
		/// </summary>
		[Required(ErrorMessage = "请输入创建时间")]
		public DateTime CreatedDate {get;set;}

		/// <summary>
		/// 备注：奖励情况
		/// </summary>
		[MaxLength(500)]
		public String Remark {get;set;}

		/// <summary>
		/// 返利情况
		/// </summary>
		[Required(ErrorMessage = "请输入返利情况")]
		[MaxLength(255)]
		public String RebateDesc {get;set;}

		/// <summary>
		/// 分享来源标识：1推广分享,2邀请赢话费
		/// </summary>
		[Required(ErrorMessage = "请输入分享来源标识：")]
		public Int32 InviteType {get;set;}
        /// <summary>
		/// 邀请人用户
		/// </summary>
        [NotMapped]
        public String InviteUserName { get; set; }
        /// <summary>
        /// 注册用户
        /// </summary>
        [NotMapped]
        public String UserName { get; set; }

    }
}

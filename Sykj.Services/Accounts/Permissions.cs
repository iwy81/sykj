﻿
using System.Collections.Generic;
using System.Linq;

namespace Sykj.Services
{
    /// <summary>
    /// 权限菜单
    /// </summary>
    public class Permissions : Sykj.Repository.RepositoryBase<Sykj.Entity.Permissions>,Sykj.IServices.IPermissions
    {
        public Permissions(Sykj.Repository.SyDbContext dbcontext) : base(dbcontext)
        {

        }

        #region 组装树
        /// <summary>
        /// 组装树
        /// </summary>
        /// <returns></returns>
        public List<Sykj.ViewModel.ZTreeModel> GetTreeList()
        {
            List<Sykj.Entity.Permissions> list = base.GetList().ToList();

            List<Sykj.ViewModel.ZTreeModel> treeList = new List<ViewModel.ZTreeModel>();
            if (list.Count > 0)
            {
                Sykj.ViewModel.ZTreeModel model;
                foreach (var item in list)
                {
                    model = new ViewModel.ZTreeModel();
                    model.id = item.PermissionId;
                    model.name = item.Title;
                    model.open = true;
                    model.isParent = false;
                    model.pId = item.ParentId;
                    treeList.Add(model);
                }
            }
            return treeList;
        }
        #endregion
    }
}

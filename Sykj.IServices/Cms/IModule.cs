﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sykj.IServices
{
    /// <summary>
    /// 内容模型
    /// </summary>
    public interface IModule : Sykj.Repository.IRepository<Sykj.Entity.Module>
    {

    }
}

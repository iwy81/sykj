﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace Sykj.Services
{
    /// <summary>
    /// 内容模型
    /// </summary>
    public class Module : Sykj.Repository.RepositoryBase<Sykj.Entity.Module>,Sykj.IServices.IModule
    {
        public Module(Sykj.Repository.SyDbContext dbcontext) : base(dbcontext)
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sykj.IServices
{
    /// <summary>
    /// 广告内容
    /// </summary>
    public interface IAdvertisement : Sykj.Repository.IRepository<Sykj.Entity.Advertisement>
    {
        /// <summary>
        /// 根据ID获取指定条数记录
        /// </summary>
        /// <param name="adId"></param>
        /// <param name="top"></param>
        /// <returns></returns>
        List<Sykj.Entity.Advertisement> GetAdPostion(int adId, int top = 0);
    }
}

﻿
using System.Collections.Generic;
using System.Linq;

namespace Sykj.Services
{
    /// <summary>
    /// 角色对应权限
    /// </summary>
    public class Rolepermissions : Sykj.Repository.RepositoryBase<Sykj.Entity.Rolepermissions>, Sykj.IServices.IRolepermissions
    {
        public Rolepermissions(Sykj.Repository.SyDbContext dbcontext) : base(dbcontext)
        {

        }

        #region 根据角色对应所有权限
        ///// <summary>
        ///// 根据角色对应所有权限
        ///// </summary>
        ///// <returns></returns>
        public List<Sykj.Entity.Rolepermissions> GetRolePermissions()
        {
            var query = from rolePermissions in _dbContext.Rolepermissions
                        join permissions in _dbContext.Permissions
                        on rolePermissions.PermissionId equals permissions.PermissionId
                        select new { rolePermissions.RoleId, permissions.PermissionId, permissions.Url };

            List<Sykj.Entity.Rolepermissions> modelList = new List<Entity.Rolepermissions>();
            if (query.Any())
            {
                Sykj.Entity.Rolepermissions model = null;
                foreach (var item in query)
                {
                    model = new Entity.Rolepermissions();
                    model.PermissionId = item.PermissionId;
                    model.RoleId = item.RoleId;
                    model.Url = item.Url;
                    modelList.Add(model);
                }
            }
            return modelList;
        }
        #endregion
    }
}

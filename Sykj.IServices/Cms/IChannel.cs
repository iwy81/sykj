﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sykj.IServices
{
    /// <summary>
    /// 栏目信息
    /// </summary>
    public interface IChannel : Sykj.Repository.IRepository<Sykj.Entity.Channel>
    {
        /// <summary>
        /// 创建栏目
        /// </summary>
        /// <param name="model">栏目模型</param>
        /// <returns>成功返回分类Id，失败返回0</returns>
        bool CreateChannel(Entity.Channel model);
    }
}

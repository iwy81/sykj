﻿
using System.Collections.Generic;

namespace Sykj.IServices
{
    /// <summary>
    /// 
    /// </summary>
    public interface IConfigsys : Sykj.Repository.IRepository<Sykj.Entity.Configsys>
    {
        /// <summary>
        /// key是否存在
        /// </summary>
        /// <param name="keyName">key</param>
        /// <returns></returns>
        bool IsExists(string keyName);

        /// <summary>
        /// 获取数据列表从缓存
        /// </summary>
        /// <returns></returns>
        List<Sykj.Entity.Configsys> GetListByCache();

        /// <summary>
        /// 根据key获取vlaue
        /// </summary>
        /// <param name="keyName"></param>
        /// <returns></returns>
        string GetValue(string keyName);

        /// <summary>
        /// 根据type获取vlaue的列表
        /// </summary>
        /// <param name="type">类型值</param>
        /// <returns></returns>
        List<Sykj.Entity.Configsys> GetValuesByType(int type);

        /// <summary>
        /// 根据key获取vlaue
        /// </summary>
        /// <param name="keyName"></param>
        /// <returns></returns>
        bool GetBoolValue(string keyName);
    }
}

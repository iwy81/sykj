﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Sykj.Repository
{
    public class RolesMap : IEntityTypeConfiguration<Sykj.Entity.Roles>
    {
        public void Configure(EntityTypeBuilder<Sykj.Entity.Roles> entity)
        {
            entity.HasKey(e => e.RoleId);

            entity.ToTable("accounts_roles");

        }
    }
}

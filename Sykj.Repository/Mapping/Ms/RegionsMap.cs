﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Sykj.Repository
{
	/// <summary>
	/// 省市区的映射
	/// </summary>
	public class RegionsMap : IEntityTypeConfiguration<Sykj.Entity.Regions>
    {
        public void Configure(EntityTypeBuilder<Sykj.Entity.Regions> entity)
        {
            entity.HasKey(e => e.AreaId);

            entity.ToTable("ms_regions");
        }
    }
}
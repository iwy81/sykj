﻿using Microsoft.EntityFrameworkCore;
using Sykj.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Sykj.Services
{
    /// <summary>
    /// 省市区的实现
    /// </summary>
    public class Regions : Sykj.Repository.RepositoryBase<Sykj.Entity.Regions>, Sykj.IServices.IRegions
    {
        ICacheService _cacheService;

        public Regions(Sykj.Repository.SyDbContext dbcontext, ICacheService cacheService) : base(dbcontext)
        {
            _cacheService = cacheService;
        }

        /// <summary>
        /// 从缓存中获取数据所有集合
        /// </summary>
        /// <returns>返回满足查询条件的list</returns>
        public List<Sykj.Entity.Regions> GetListByCache()
        {
            List<Sykj.Entity.Regions> list = new List<Entity.Regions>();

            if (_cacheService.Exists(CacheKey.NEWREGIONSLIST))
            {
                list = _cacheService.GetCache<List<Sykj.Entity.Regions>>(CacheKey.NEWREGIONSLIST);
            }
            else
            {
                int[] array = new[] {1,2,3 };
                list = GetList(x => array.Contains(x.Depth)).AsNoTracking().ToList();//dbcontext不进行跟踪，去缓存
                _cacheService.SetCache(CacheKey.NEWREGIONSLIST, list);
            }
            return list;
        }

        /// <summary>
        /// 获得省市区缓存模型
        /// </summary>
        /// <param name="predicate">参数</param>
        /// <returns></returns>
        public Sykj.Entity.Regions GetModelByCache(Func<Entity.Regions, bool> predicate)
        {
            List<Sykj.Entity.Regions> list = GetListByCache();
            return list.FirstOrDefault(predicate);
        }

        /// <summary>
        /// 获取某区域ID下的路径
        /// </summary>
        /// <param name="regionId"></param>
        /// <returns>返回路径</returns>
        public string GetPath(string regionId)
        {
            //缓存中取出所有的
            List<Sykj.Entity.Regions> areaAllList = GetListByCache();
            Sykj.Entity.Regions model = areaAllList.Where(c => c.RegionId == regionId).FirstOrDefault();
            if (model != null)
            {
                return model.Path;
            }
            else
            {
                return "";
            }
        }

        /// <summary>
        /// 根据任何地区ID，获取地区完整名称集合
        /// </summary>    
        ///<param name="regionId"></param> 
        ///<return>返回名称</return>
        public List<string> GetRegionNameByRID(string regionId)
        {
            //缓存中取出所有的
            List<Sykj.Entity.Regions> areaAllList = GetListByCache();

            string path = GetPath(regionId) + regionId.ToString();
            //数组类型转化
            string[] pathArr = path.Split('|');

            var result = from q in areaAllList where pathArr.Contains(q.RegionId) select q.RegionName;
            return result.ToList();
        }

        /// <summary>
        /// 根据任何地区ID，获取地区完整名称
        /// </summary>
        /// <param name="regionId">regionId</param>
        /// <param name="sep">拼接字符</param>
        /// <returns></returns>
        public string GetFullNameById(string regionId, string sep = "")
        {
            List<string> tmp = GetRegionNameByRID(regionId);
            if (tmp != null && tmp.Count > 0)
            {
                return string.Join(sep, tmp);
            }
            return string.Empty;
        }

        /// <summary>
        /// 根据regionId获取省份名称
        /// </summary>
        /// <param name="regionId"></param>
        /// <returns></returns>
        public string GetProvinceName(string regionId)
        {
            string result = string.Empty;

            var regionInfo = GetModelByCache(c => c.RegionId == regionId);
            if (regionInfo == null)
                result = "";

            if (regionInfo.Depth>1)
            {
                //获取顶级regionId
                string topRegionId = regionInfo.Path.Split(new[] { '|' })[1];
                var model = GetModelByCache(c => c.RegionId == topRegionId);
                if (model != null)
                {
                    result = model.RegionName;
                }
            }
            else
            {
                result = regionInfo.RegionName;
            }
            return result;
        }

        /// <summary>
        /// 根据regionId获取城市名称
        /// </summary>
        /// <param name="regionId"></param>
        /// <returns></returns>
        public string GetCityName(string regionId)
        {
            string result = string.Empty;

            var regionInfo = GetModelByCache(c => c.RegionId == regionId);
            if (regionInfo == null)
                result = "";

            if (regionInfo.Depth > 2)
            {
                //获取顶级regionId
                string topRegionId = regionInfo.Path.Split(new[] { '|' })[2];
                var model = GetModelByCache(c => c.RegionId == topRegionId);
                if (model != null)
                {
                    result = model.RegionName;
                }
            }
            else
            {
                result = regionInfo.RegionName;
            }
            return result;
        }
    }
}
/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：                                                    
*│　作    者：lh                                              
*│　版    本：1.0   模板代码自动生成                                              
*│　创建时间：2019-02-20 11:49:47 
*│  公    司：云南尚雅科技文化有限公司
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间: Sykj.Repository                                  
*│　类    名：Guestbook                                     
*└──────────────────────────────────────────────────────────────┘
*/
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Sykj.Repository
{
	/// <summary>
	/// 留言信息表
	/// </summary>
	public class GuestbookMap : IEntityTypeConfiguration<Sykj.Entity.Guestbook>
    {
        public void Configure(EntityTypeBuilder<Sykj.Entity.Guestbook> entity)
        {
            entity.HasKey(e => e.Id);

            entity.ToTable("ms_guestbook");
        }
    }
}

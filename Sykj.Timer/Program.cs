﻿
using System;
using Quartz.Impl;
using Quartz;

namespace Sykj.Timer
{
    public class Program
    {
        static void Main(string[] args)
        {
            //调度器,生成实例的时候线程已经开启了，不过是在等待状态
            StdSchedulerFactory factory = new StdSchedulerFactory();
            IScheduler scheduler = factory.GetScheduler().Result;

            //将job和trigger注册到scheduler中
            scheduler.ScheduleJob(MyJob.GetJobDetail(), MyJob.GetTrigger());

            scheduler.ScheduleJob(OrderJob.GetJobDetail(), OrderJob.GetTrigger());

            //start让调度线程启动【调度线程可以从jobstore中获取快要执行的trigger,然后获取trigger关联的job，执行job】
            scheduler.Start();

            Console.ReadKey();
        }
    }
}

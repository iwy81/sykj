/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：                                                    
*│　作    者：bjg                                             
*│　版    本：1.0   模板代码自动生成                                              
*│　创建时间：2019-02-21 09:38:35     
*│  公    司：云南尚雅科技文化有限公司
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Sykj.IServices                                   
*│　接口名称： IFavorite                                   
*└──────────────────────────────────────────────────────────────┘
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sykj.IServices
{
    /// <summary>
	/// 收藏信息表
	/// </summary>
    public interface IFavorite: Sykj.Repository.IRepository<Sykj.Entity.Favorite>
    {
        /// <summary>
        /// 分页查询扩展(高校) bjg
        /// </summary>
        /// <param name="pageIndex">当前页</param>
        /// <param name="pageSize">页大小</param>
        /// <param name="predicate">条件</param>
        /// <param name="ordering">排序</param>
        /// <param name="args">参数</param>
        /// <returns></returns>
        IQueryable<Entity.Favorite> GetGuestBookPagedList(int pageIndex, int pageSize, string predicate, string ordering, params object[] args);
    }
}
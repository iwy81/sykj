﻿/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：{Comment}                                                    
*│　作    者：{Author}                                            
*│　版    本：1.0    模板代码自动生成                                                
*│　创建时间：{GeneratorTime}       
*│  公    司：云南尚雅科技文化有限公司
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： {ServicesNamespace}                                  
*│　类    名： {ModelName}                                 
*└──────────────────────────────────────────────────────────────┘
*/
using System;
using System.Collections.Generic;
using System.Text;

namespace {ServicesNamespace}
{
	/// <summary>
	/// {Comment}
	/// </summary>
    public class {ModelName} : Sykj.Repository.RepositoryBase<Sykj.Entity.{ModelName}>,Sykj.IServices.I{ModelName}
    {
        public {ModelName}(Sykj.Repository.SyDbContext dbcontext) : base(dbcontext)
        {
		    
        }
    }
}
﻿@{
    ViewData["Title"] = "{Comment}列表";
    Layout = "~/Areas/Manager/Views/Shared/_Layout.cshtml";
}
<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-card">
                <div class="layui-card-header">{Comment}</div>
                <div class="layui-card-body">
                    <div class="layui-row">
                        <div class="layui-form-item">
                            <div class="layui-input-inline">
                                <input type="text" id="keyWords" name="keyWords" placeholder="请输入关键字" autocomplete="off" class="layui-input">
                            </div>
                            <div class="layui-inline">
                                <button class="layui-btn layuiadmin-btn-replys" id="search"><i class="layui-icon layui-icon-search layuiadmin-button-btn"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="layui-row">
                        <div class="layui-btn-container">
                            <div class="layui-btn-group">
                                <button class="layui-btn layui-btn-sm" id="add"><i class="layui-icon">&#xe654;</i>增加</button>
                                <button class="layui-btn layui-btn-sm" id="deleteAll"><i class="layui-icon">&#xe640;</i>删除</button>
                            </div>
                        </div>
                    </div>
                    <div class="layui-row">
                        <table class="layui-table" lay-data="{page:true, id:'list'}" lay-filter="list">
                            <thead>
                                <tr>
                                    <th lay-data="{checkbox:true, fixed: true}"></th>
		{ColumnList}
                                    <th lay-data="{toolbar:'#tableoption'}">操作</th>
                                </tr>
                            </thead>
                        </table>
                        <script type="text/html" id="tableoption">
                            <a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="edit"><i class="layui-icon layui-icon-edit"></i>编辑</a>
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@section scriptsfeet
{
<script type="text/javascript">
        layui.config({
            base: '/layuiadmin/' //自定义模块路径
        }).use(['sykjwh', 'table'], function () {
            var $ = layui.$, sykjwh = layui.sykjwh, table = layui.table;

            //绑定数据
            sykjwh.dataBind('@Url.Action("list", "{ModelName}", new { area = Constant.AREAMANAGER })');

            //添加
            $("#add").click(function () {
                sykjwh.open('添加{Comment}', '@Url.Action("add", "{ModelName}", new { area = Constant.AREAMANAGER })', 700, 400);
            });

            //批量删除
            $("#deleteAll").click(function () {
                sykjwh.batOperate('@Url.Action("deleteall", "{ModelName}", new { area = Constant.AREAMANAGER })', '{PrimaryKey}');
            });

            //监听表格行
            table.on('tool(list)', function (obj) {
                var data = obj.data;//获得当前行数据
                if (obj.event === 'edit') {
                    sykjwh.open('编辑{Comment}', '@Url.Action("edit", "{ModelName}", new { area = Constant.AREAMANAGER })/' + data.{PrimaryKey}, 700, 400);
                }
            });

            //搜索
            $("#search").click(function () {
                //绑定数据
                sykjwh.dataBind('@Url.Action("list", "{ModelName}", new { area = Constant.AREAMANAGER })', {
                    keyWords: $.trim($("#keyWords").val())
                });
            });
        });
</script>
}
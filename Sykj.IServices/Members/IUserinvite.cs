/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：推广表                                                    
*│　作    者：bjg                                             
*│　版    本：1.0   模板代码自动生成                                              
*│　创建时间：2019-04-03 10:51:43     
*│  公    司：云南尚雅科技文化有限公司
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Sykj.IServices                                   
*│　接口名称： IUserinvite                                   
*└──────────────────────────────────────────────────────────────┘
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sykj.IServices
{
    /// <summary>
	/// 推广表
	/// </summary>
    public interface IUserinvite : Sykj.Repository.IRepository<Sykj.Entity.Userinvite>
    {
        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="pageIndex">当前页</param>
        /// <param name="pageSize">页大小</param>
        /// <param name="predicate">条件</param>
        /// <param name="ordering">排序</param>
        /// <param name="args">参数</param>
        /// <returns></returns>
        IQueryable<Sykj.Entity.Userinvite> GetPagedListExt(int pageIndex, int pageSize, string predicate, string ordering, params object[] args);

        /// <summary>
        /// 获得记录总数
        /// </summary>
        /// <param name="predicate">条件</param>
        /// <param name="args">参数</param>
        /// <returns></returns>
        int GetCountExt(string predicate, params object[] args);


        /// <summary>
        /// 获取分享邀请注册赢话费活动线上成功购买用户数量
        /// </summary>
        /// <param name="userId">邀请者id</param>
        /// <returns></returns>
        int OnlineBuyNum(int userId);
    }
}
﻿using Microsoft.Extensions.Logging;

namespace Sykj.Repository
{
    /// <summary>
    /// EF日志工厂
    /// </summary>
    public class EFLoggerFactory: ILoggerFactory
    {
        public void AddProvider(ILoggerProvider provider)
        {
        }

        public ILogger CreateLogger(string categoryName)
        {
            return new EFLogger(categoryName);//创建EFLogger类的实例
        }

        public void Dispose()
        {

        }
    }
}

﻿
using System;

namespace Sykj.CodeGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Console.WriteLine("代码生成开始...");

            CodeGenerateOption options = new CodeGenerateOption();

            options.ConnectionString = "Server=10.10.3.101;UserId=root;PassWord=root!@#;Database=open;Charset=utf8;";
            options.DbType = DatabaseType.MySQL;//数据库类型

            //options.ConnectionString = "server=10.10.3.101;database=open;uid=sa;pwd=123456;";
            //options.DbType = DatabaseType.SqlServer;//数据库类型

            //options.ConnectionString = "Host=localhost;Database=open;Username=postgres;Password=123456;";
            //options.DbType = DatabaseType.PostgreSQL;//数据库类型

            options.Author = "lh";//作者名称
            options.ModelsNamespace = "Sykj.Entity";//实体命名空间
            options.IServicesNamespace = "Sykj.IServices";//服务接口命名空间
            options.ServicesNamespace = "Sykj.Services";//服务命名空间

            CodeGenerator codeGenerator = new CodeGenerator(options);
            codeGenerator.GenerateTemplateCodesFromDatabase(true);

            Console.WriteLine("代码生成完成!");
            
            Console.Read();
        }
    }
}

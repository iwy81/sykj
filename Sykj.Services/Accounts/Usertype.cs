﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace Sykj.Services
{
    /// <summary>
    /// 用户类型
    /// </summary>
    public class Usertype : Sykj.Repository.RepositoryBase<Sykj.Entity.Usertype>,Sykj.IServices.IUsertype
    {
        public Usertype(Sykj.Repository.SyDbContext dbcontext) : base(dbcontext)
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sykj.ViewModel
{
    public class HomeModel
    {
        public Entity.Users AccountsUser { get; set; }

        public List<Entity.Permissions> PermissionsList { get; set; }
    }
}
